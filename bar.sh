#!/bin/sh
while true; do
    temp="$(sysctl -n hw.sensors.acpithinkpad0.temp0 | sed -rn 's/(.*) degC/\1°C/p')"
    bat="$(apm -l)%"
    vol="$(mixerctl -n outputs.master)"
    if [ "$(mixerctl -n outputs.master.mute)" == "on" ]; then
        vol="${vol} (mute)"
    fi
    echo "${temp}  Bat:${bat}  Vol:${vol}"
    sleep 1
done
