#!/bin/sh
set errexit
dotfiles -s -C ./dotfilesrc
if [ ! -d ~/.vim ]; then
	mkdir -p ~/.vim/bundle
	git clone https://github.com/gmarik/vundle.git ~/.vim/bundle/vundle
        vim +:BundleInstall +:qa
fi
