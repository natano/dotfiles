set nocompatible
filetype off
set rtp+=~/.vim/bundle/vundle
call vundle#rc()

Bundle 'kien/ctrlp.vim'
Bundle 'tpope/vim-commentary'
Bundle 'vim-scripts/lout.vim'

syntax on
filetype plugin indent on

set smarttab
set nojoinspaces
set ruler
set history=1000
set hlsearch
set autoindent
set number
set modelines=0
set backspace=indent,eol,start
set cryptmethod=blowfish
set wildmode=longest,list
set wildignore=*.o,*.pyc,*/__pycache__/**,*/env/**,*/vendor/**,*/build/**
set tabpagemax=100

autocmd Filetype python setl et sw=4
autocmd Filetype ruby setl et sw=2
autocmd FileType tex setl makeprg=latexmk\ -pdf\ -pdflatex=\"pdflatex\ -file-line-error\ -interaction=nonstopmode\"
" autocmd BufWritePost *.tex silent make!

colo desert
